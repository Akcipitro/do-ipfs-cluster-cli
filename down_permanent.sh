#! /bin/bash

if [[ ! $1 ]]; then
    echo "please input tag_name of Droplet group"
    exit
else
    NAME=$1
fi

if [[ $(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets?tag_name=$NAME" | jq '.droplets[0].id') ]]; then
    curl -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets?tag_name=$NAME"
fi
