#! /bin/bash

mkdir $HOME/go
source $HOME/.bashrc
go get -u github.com/ipfs/ipfs-update
ipfs-update install latest
ipfs init
