#! /bin/bash

# if [[ ! $1 ]]; then
#     echo "a name argument is required"
#     exit
# else
#     NAME=$1
# fi
# if [[ ! $2 ]]; then
#     SERVERS=3
# else
#     SERVERS=$2
# fi

check_address ()
{
    # ends when script can retrieve address of host
    while [[ ! ${ADDRESSES[$i]} ]]; do
	echo "reattempting address retrieval"
	ADDRESSES[$i]=$(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets/${NEW_IDS[$i]}" | jq '.droplet.networks.v4[0].ip_address' | tr -d '"')
    done
    echo '"Host '$i' address is '${ADDRESSES[$i]}'"'
}

check_id ()
{
    # ends when new host POST request returns successful
    while [[ ! ${NEW_IDS[$i]} ]]; do
	echo "reattempting host launch"
	NEW_IDS[$i]=$(curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"name":"'$NAME'.'$i'","region":"'${REGIONS[$i]}'","size":"s-1vcpu-1gb","image":"fedora-27-x64","ssh_keys":["'$SSH_KEY_ID'"],"monitoring":"True","tags":["'$NAME'"]}' "https://api.digitalocean.com/v2/droplets" | jq '.droplet.id')
    done
    echo '"Host '$i' Droplet ID is '${NEW_IDS[$i]}'"'
}

# gets number of unique regions where DO offers servers
REGIONS_MAX=$(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/regions" | jq '.meta.total')
# dynamically sets indexes in array to region slugs
for (( i=0; i<$REGIONS_MAX; i++ )); do
    REGIONS[$i]=$(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/regions" | jq '.regions['$i'].slug')
done

echo "DigitalOcean IPFS Cluster Setup"

echo "Please enter a name for the cluster."
read NAME

echo "How many instances do you want set up?"
read SERVERS
# @todo:catch incorrect input
while [[ ! $SERVERS =~ [0-9][0]* ]]; do
    echo "Please enter a valid number from 1-10"
    read SERVERS
done

# primary loop for IPFS node setup 
for (( i=0; i<$REGIONS_MAX; i++ )); do
    # launches DigitalOcean droplet
    NEW_IDS[$i]=$(curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"name":"'$NAME'.'$i'","region":"'${REGIONS[$i]}'","size":"s-1vcpu-1gb","image":"fedora-27-x64","ssh_keys":["'$SSH_KEY_ID'"],"monitoring":"True","tags":["'$NAME'"]}' "https://api.digitalocean.com/v2/droplets" | jq '.droplet.id')
    if [[ ! ${NEW_IDS[$i]} ]]; then
	check_id
    fi

    # waits until droplet is active
    while [[ $(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets/${NEW_IDS[$i]}" | jq '.droplet.status') != '"active"' ]]; do
	echo "Host not fully up yet"
	sleep 30s
    done

    # stores IP address of droplet
    ADDRESSES[$i]=$(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets/${NEW_IDS[$i]}" | jq '.droplet.networks.v4[0].ip_address' | tr -d '"')
    echo ${ADDRESSES[$i]}
    if [[ ! ${ADDRESSES[$i]} ]]; then
	check_address
    fi

    # boostraps full node on remote server
    while [[ ! $(nc -i 1 ${ADDRESSES[$i]} 22) ]]; do
	sleep 30s
    done
    scp -i $KEY_FILENAME -o StrictHostKeyChecking=no bootstrap1.sh root@${ADDRESSES[$i]}:/tmp/bootstrap1.sh
    ssh -i $KEY_FILENAME -o StrictHostKeyChecking=no root@${ADDRESSES[$i]} "sh /tmp/bootstrap1.sh"
    scp -i $KEY_FILENAME -o StrictHostKeyChecking=no bootstrap2.sh ipfs@${ADDRESSES[$i]}:/tmp/bootstrap2.sh
    ssh -i $KEY_FILENAME -o StrictHostKeyChecking=no ipfs@${ADDRESSES[$i]} "sh /tmp/bootstrap2.sh"
done
