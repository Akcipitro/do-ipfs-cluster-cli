#! /bin/bash

dnf -y update
dnf -y install git golang
adduser ipfs
echo 'export GOPATH=$HOME/go' >> /home/ipfs/.bashrc
echo 'export PATH=$PATH:$HOME/go/bin' >> /home/ipfs/.bashrc
su ipfs -c "mkdir /home/ipfs/.ssh"
mv $HOME/.ssh/authorized_keys /home/ipfs/.ssh/authorized_keys
chown ipfs /home/ipfs/.ssh/authorized_keys
